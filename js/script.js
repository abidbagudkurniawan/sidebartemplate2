function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
    document.getElementById("main").style.marginLeft = "300px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
}

$(document).ready(function(){
	$('#list-toggle-gallery').mouseenter(function(){
		$('.toggle-gallery').slideDown(400);
	});
	$('#list-toggle-jobs').mouseleave(function(){
		$('.toggle-gallery').slideUp(400);
	});

	$('#list-toggle-jobs').mouseenter(function(){
		$('.toggle-job').slideDown(400);
	});
	$('#list-toggle-jobs').mouseleave(function(){
		$('.toggle-job').slideUp(400);
	});
});